package com.xnx3.spider.vo;

import com.xnx3.BaseVO;

/**
 * 请求响应
 * @author 管雷鸣
 *
 */
public class ResponseVO extends BaseVO{
	
	private String mimeType; //资源类型，比如  text/css

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	
	
}
